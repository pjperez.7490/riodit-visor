##### Ejecutar las migraciones

```
	$ php artisan migrate:fresh --seed 
```

##### Ejecutar comando

```
	$ php artisan passport:install --force 
```

**Recomendaciones**

Crear usuario con un email real para probar que se envía la contraseña al correo electrónico correctamente la útilizar esta funcionalidad desde la app.

Ruta para el registro
```
  http://localhost/riodit_servidor/public/api/register
```

Cuerpo
```
  {
    "name": "",
    "email": "",
    "password": "",
    "confirm_password": ""
  }
```

También puede crear un usuario agregando el el seed
```
  database
```


**Recomendaciones**

Agregar configuración del servicio smtp, como prueba utilize mi gmail, en el archivo *** .env.example +** deje la configuración solo pendiente de rellenar los campos de usuario y contraseña para el gmail.