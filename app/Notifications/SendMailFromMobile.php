<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;

class SendMailFromMobile extends Notification
{
    use Queueable;

    protected $user;
    protected $asunto;
    protected $mensaje;
    protected $telefono;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $asunto = '', $mensaje = '', $telefono = '')
    {
        $this->user = $user;
        $this->asunto = $asunto;
        $this->mensaje = $mensaje;
        $this->telefono = $telefono;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mailMessage = new MailMessage;
        $cliente = $this->user->cliente;

        $mailMessage->subject($this->asunto)
                    ->line('Usuario: '. $this->user->email)
                    ->line('Cliente: '. $cliente->nombre)
                    ->line('Rut cliente: '. $cliente->rut)
                    ->line('Email cliente: '. $cliente->email)
                    ->line('Celular cliente: '. $cliente->celular)
                    ->line('Asunto: '. $this->asunto)
                    ->line('Mensaje: '. $this->mensaje);

        if ($this->telefono && $this->telefono != '') {
            $mailMessage->line('Teléfono: '. $this->telefono);
        }
                    
        $mailMessage->line('Enviado desde la aplicación móvil.');

        return $mailMessage;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
