<?php

namespace App\Http\Controllers\Admin;

use App\Carpeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InicioController extends Controller
{
    public function visor(Request $request)
    {        
        $carpetas = Carpeta::with('cliente', 'estados')->whereHas('estados', function($q){
            $q->where('estado_carpeta_preset_id', 4);
        })->orderBy('id', 'desc')->get()->filter(function($carpeta) {
            $estadoCanalVerde = $carpeta->estados->where('estado_carpeta_preset_id', 4)->first();

            // se obtiene la diferencia en horas de la fecha de creación a la actulidad
            $diff = $estadoCanalVerde->created_at->diffInHours( now() );            
           
            // comparamos que no sea mayor a 72 horas(3 días)
            return $diff <= 72;
        });

        $carpetas = array_values($carpetas->toArray());

        if ($request->ajax()) {
            return response()->json([
                'carpetas' => $carpetas
            ], 200);
        }

        return view('admin.inicio.visor');
    }

}
