<?php

namespace App\Http\Requests;

use App\Http\Requests\CreateCarpetaRequest;

class UpdateCarpetaRequest extends CreateCarpetaRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->reglas['numero_carpeta'] = 'required|unique:carpetas,numero_carpeta,'.$this->carpeta->id.','.'id';
        
        return $this->reglas;
    }
}
