<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCarpetaRequest extends FormRequest
{
    protected $reglas = [
        'numero_carpeta'   => 'required|max:255|unique:carpetas',
        'cliente_id' => 'required|exists:clientes,id',
        'punto_de_carga_id' => 'required|exists:puntos_cargas,id',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->reglas;
    }
}
