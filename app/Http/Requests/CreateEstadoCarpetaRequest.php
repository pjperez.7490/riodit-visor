<?php

namespace App\Http\Requests;

use App\EstadoCarpetaPreset;
use Illuminate\Foundation\Http\FormRequest;

class CreateEstadoCarpetaRequest extends FormRequest
{
    protected $reglas = [
        'nombre_clave' => 'required',
        'nombre_mostrar' => 'required',
        'descripcion' => 'required',
        'fecha_evento' => 'required',
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->reglas;
    }

    public function getValidatorInstance()
    {
        $this->addNombreClaveAndNombreMostrar();
        return parent::getValidatorInstance();
    }

    protected function addNombreClaveAndNombreMostrar()
    {

        $estadoCarpetaPreset = EstadoCarpetaPreset::find($this->request->get('estado_carpeta_preset_id'));

        $this->request->add([
            'nombre_clave' => $estadoCarpetaPreset->nombre_clave,
            'nombre_mostrar' => $estadoCarpetaPreset->nombre_mostrar,
            'descripcion' => $estadoCarpetaPreset->descripcion,
        ]);
    }
}
