<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClienteRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       $reglas = [
            'nombre'  => 'required|string|max:100',  
            'rut'     => 'required|unique:clientes',
            'user_id' => 'required|exists:users,id',
        ];

        if($this->email !=null){
          $reglas['email'] =  'email|max:255|unique:clientes';
        }
    
        return $reglas;
    }

    public function getValidatorInstance()
    {
        // add user_id
        $user = auth()->user();
        $this->request->add(['user_id' => $user->id]);
        return parent::getValidatorInstance();
    }
}
