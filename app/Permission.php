<?php

namespace App;


use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $guarded = ['id','created_at','updated_at'];

    protected $table = 'permissions';

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
}
