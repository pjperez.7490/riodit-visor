<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EstadoCarpeta extends Model
{
    protected $table = 'estado_carpetas';
    protected $fillable = ['nombre_mostrar', 'nombre_clave', 'descripcion', 'fecha_evento', 'estado_carpeta_preset_id', 'carpeta_id'];
    
    // relationships
    public function carpeta()
    {
        return $this->belongsTo(Carpeta::class);
    }
    
    public function preset()
    {
        return $this->belongsTo(EstadoCarpetaPreset::class, 'estado_carpeta_preset_id');
    }

    public function creadoPor()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    // mutators
    public function getFechaEventoAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i');
    }

    public function setFechaEventoAttribute($value)
    {
        if(is_string($value)){
            $this->attributes['fecha_evento'] = Carbon::createFromFormat('d/m/Y H:i', $value)->format('Y-m-d H:i');
        }else{
            $this->attributes['fecha_evento'] =  $value;
        }
    }

    // events
    public static function boot()
    {
        parent::boot();

        self::creating(function($estadoCarpeta){
            $estadoCarpeta->notificado = false;
            $estadoCarpeta->user_id = optional(auth()->user())->id;
        });
    }

}
