<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{
    protected $table = 'push_notifications';
    protected $fillable = ['token', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
