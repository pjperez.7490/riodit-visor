<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCarpetaPreset extends Model
{
    protected $table = 'estado_carpeta_presets';

    // methods
    public function isActive(Carpeta $carpeta)
    {
        $result = $carpeta->estados->where('estado_carpeta_preset_id', $this->id)->first();

        return $result? true: false;
    }

    // scopes
    public function scopePorOrden($query)
    {
        return $query->orderBy('orden', 'asc');
    }

    public function scopeNoAutomatico($query)
    {
        return $query->whereAutomatico(false);
    }

    public function scopeOrdenNoNull($query)
    {
        return $query->where('orden', '!=', null);
    }

}
