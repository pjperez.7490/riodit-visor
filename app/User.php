<?php

namespace App;

use App\Permission;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres', 'email', 'password', 'apellidos', 'telefono', 'estado', 'cliente_id', 'token_push_notification'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['rol', 'nombre_completo'];

    public function getRol()
    {
        $rol = $this->roles()->first();
        return $rol;
    }

    public function getRolId()
    {
        $rol = $this->roles()->first();
        if($rol){
            return $rol->id;
        }
        return null;
    }

    public function getTokens()
    {
        return $this->pushNotifications->map(function($item){
            return $item->token;
        })->toArray();
    }

    // relationships
    public function pushNotifications(Type $var = null)
    {
        return $this->hasMany(PushNotification::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function carpetas()
    {
        return $this->hasMany(Carpeta::class, 'cliente_id', 'cliente_id');
    }

    // mutators
    public function getRolAttribute()
    {
        $rol_name = '';
        $rol = $this->roles()->first();
        if ($rol) {
            $rol_name = $rol->display_name;
        }
        return ucwords($rol_name);
    }

    public function getNombreCompletoAttribute()
    {
    	return $this->getNombreCompleto();
    }


    public function permiso($user_id, $permiso)
    {
        $user = $this;
        if ($user_id == $user->id || $user->can($permiso)) {
            return true;
        } else {
            return false;
        }
    }

    public function canPermissionById($permission_id)
    {
        $permission = Permission::find($permission_id);

        if ($permission && $this->can($permission->name) ) return true;

        return false;
    }

    public function verificarRol($rol_name)
    {
        if( $this->roles->where('name',$rol_name)->count() > 0){
            return true;
        }

        return false;
    }

    public function getNombreCompleto()
    {
    	return ucwords("$this->nombres $this->apellidos");
    }
}
