<?php

namespace App;

use Carbon\Carbon;
use App\EstadoCarpetaPreset;
use App\Notifications\AccountApproved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Carpeta extends Model
{
    protected $table = 'carpetas';
    protected $fillable = [
        'numero_carpeta',
        'cliente_id',
        'punto_de_carga_id',
        'user_id',
        'referencia'
    ];
    protected $appends = ['fecha', 'transporte', 'estado', 'finalizada', 'color'];

    public function tieneEstado($estado_id)
    {
        $result = $this->estados->where('estado_carpeta_preset_id', $estado_id)->first();

        return $result? true: false;
    }

    public function buttonsEstadosPreset()
    {
        $estadosPreset = EstadoCarpetaPreset::porOrden(false)->noAutomatico()->ordenNoNull()->get();
        $activeButton = false;

        foreach ($estadosPreset as $estadoPreset) {
            $tieneEstado = $estadoPreset->isActive($this);

            if (!$tieneEstado && !$activeButton) {
                $estadoPreset->disabled = true;
                $activeButton = true;
            } else {
                $estadoPreset->disabled = false;
            }

        }

        return $estadosPreset;
    }

    public function btnEstadosPreset()
    {
        $estadosPreset = EstadoCarpetaPreset::porOrden(false)->noAutomatico()->ordenNoNull()->get();
        $activeButton = false;
        $user = auth()->user();

        foreach ($estadosPreset as $estadoPreset) {
            if($user->canPermissionById($estadoPreset->permission_id) ) {
                $estadoPreset->permiso = true;
            } else {
                $estadoPreset->permiso = false;
            }

            $tieneEstado = $estadoPreset->isActive($this);

            if (!$tieneEstado && !$activeButton) {
                $estadoPreset->disabled = false;
                $activeButton = true;
            } else {
                $estadoPreset->disabled = true;
            }
        }

        return $estadosPreset;
    }

    public static function filter(array $options = [])
    {
        $carpetas = Carpeta::with(['cliente', 'creadoPor', 'puntoCarga', 'estados']);
        if ( sizeof($options) >= 0 ) {
            $fechaInicio = array_key_exists('fecha_inicio', $options) && $options['fecha_inicio'] != null ? Carbon::createFromFormat('d/m/Y', $options['fecha_inicio'])->format('Y-m-d') : null;
            $fechaFin = array_key_exists('fecha_fin', $options) && $options['fecha_fin'] != null ? Carbon::createFromFormat('d/m/Y', $options['fecha_fin'])->format('Y-m-d') : null;
            $estadoCarpetaPresetId = array_key_exists('estado_carpeta_preset_id', $options)?  $options['estado_carpeta_preset_id'] : null;
            $clienteId = $options['cliente_id'];
            
            if (!$estadoCarpetaPresetId && !$fechaInicio && !$clienteId) {
                return $carpetas->get();
            }

            if ($fechaInicio) {
                $carpetas->entreFechas($fechaInicio, $fechaFin);
            }

            if ($clienteId) {
                $carpetas->whereClienteId($clienteId);
            }

            if ($estadoCarpetaPresetId && $carpetas != null) {
                $carpetas->whereHas('estados', function($query) use($estadoCarpetaPresetId) {
                    $query->whereEstadoCarpetaPresetId($estadoCarpetaPresetId); 
                });
            }
        }

        return $carpetas->get();
    }

    // scopes
    public function scopeEntreFechas($query, $fechaInicio, $fechaFin = null)
    {
        if ($fechaFin == null)
            $fechaFin = $fechaInicio;

        return $query->whereHas('estados', function($q) use($fechaInicio, $fechaFin){
            $q->whereDate('fecha_evento', '>=', $fechaInicio)
              ->whereDate('fecha_evento', '<=', $fechaFin);
        });
    }

    // relationships
    public function creadoPor()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function puntoCarga()
    {
        return $this->belongsTo(PuntosCarga::class, 'punto_de_carga_id');
    }

    public function estados()
    {
        return $this->hasMany(EstadoCarpeta::class, 'carpeta_id', 'id');
    }

    // mutators
    public function getFechaAttribute()
    {
        $value = $this->created_at;
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function getTransporteAttribute()
    {
        return $this->puntoCarga->nombre;
    }

    public function getEstadoAttribute()
    {
        $estados = $this->estados;

        if ($estados->count() === 0) {
            return 'N/A';
        }

        return optional( $estados->sortByDesc('id')->first())->nombre_mostrar;
    }

    public function getColorAttribute()
    {
        $estados = $this->estados;
        
        if ($estados->count() === 0) {
            return '#3677C8';
        }

        $estado = $estados->sortByDesc('id')->first();

        $color = $estado->preset->color;
        
        // return '#FF0000';
        return $color;
    }

    public function getFinalizadaAttribute()
    {
        $finalizados = $this->estados->filter(function($estado){
            return $estado->nombre_clave === 'finalizada';
        });

        if ($finalizados->count()) return true;

        return false;
    }

    public function getReferenciaAttribute($value)
    {
        return $value;
        if(!$value){
            if (!$this->exists) return '';

            return 'N/A';
        };

        return $value;
    }

    // events
    public static function boot()
    {
        parent::boot();

        self::creating(function($carpeta){
            $carpeta->numero_carpeta = str_pad( $carpeta->numero_carpeta, 6, '0', STR_PAD_LEFT );

            $userId = optional(auth()->user())->id;
            $carpeta->user_id = $userId? $userId : 1;
        });

        self::created(function($carpeta){
            $user = optional($carpeta->cliente->usuarios)->first();
        });
        
        self::updated(function($carpeta){
            $carpeta->user_id = auth()->user()->id;
        });
        
        self::updating(function($carpeta){
            $carpeta->numero_carpeta = str_pad( $carpeta->numero_carpeta, 6, '0', STR_PAD_LEFT );
        });

    }
}
