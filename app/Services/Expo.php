<?php

namespace App\Services;

use GuzzleHttp\Client;

class Expo  
{
    protected $client;
    protected $params = [];
    protected $response = null;
    protected $response_data = null;
    protected $headers = [
        'accept' => 'application/json',
        'accept-encoding' => 'gzip, deflate',
        'content-type' => 'application/json',
    ];

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function sendNotification($params = [])
    {
        // $params = [
        //     [
        //         "to"=> "ExponentPushToken[SUct33AOp_2Rfne_PEquTk]",
        //         "sound"=> "default",
        //         "body"=> "Hello world!"
        //     ], 
        //     [
        //         "to"=> "ExponentPushToken[SUct33AOp_2Rfne_PEquTk]",
        //         "badge"=> 1,
        //         "body"=> "You've got mail"
        //     ], 
        //     [
        //         "to"=> [
        //             "ExponentPushToken[zzzzzzzzzzzzzzzzzzzzzz]",
        //             "ExponentPushToken[SUct33AOp_2Rfne_PEquTk]"
        //         ],
        //         "body"=> "Breaking news!"
        //     ]
        // ];

        // $params = [
        //     'body' => 'Finalizado',
        //     'sound' => 'default',
        //     'title' => 'Estado Nuevo',
        //     'to' => 'ExponentPushToken[SUct33AOp_2Rfne_PEquTk]',
        // ];

        $this->params = $params;

        if ( empty($this->params ) ) {
            return [
                'status' => 422,
            ];
        }

        $this->response = $this->client->post('send', [
            'headers' => $this->headers,
            'json' => $this->params,
        ]);

        return $this->handleResponse();
    }

    protected function handleResponse()
    {
        $data = $this->dataResponse();

        return [
            'status' => $this->response->getStatusCode(),
            'response' => $data,
            'is_assoc' => $this->is_assoc($data),
        ];
    }

    protected function dataResponse()
    {
        $this->getResponse();

        // verificando si es un array asociativo o secuencial
        if ($this->is_assoc($this->response_data)) {
            // se verifica si es un array vacio ya que se puede dar el caso
            if(empty($this->response)) {
                $response = null;
            }
            
            $response = $this->asociarParametrosConRespuesta($this->params, $this->response_data);
        } else {
            $response = $this->asociarParametrosConRespuestaMultiple($this->params, $this->response_data);
        }

        return $response;
    }

    public function asociarParametrosConRespuestaMultiple($params, $response)
    {
        $data = [];
        $dataTo = $this->getTo($params);

        foreach ($response as $index => $value ) {
            $data[] = $this->asociarParametrosConRespuesta(['to' => $dataTo[$index]], $value);
        }

        return $data;
    }

    private function getTo($params)
    {
        return $this->findDataByKey($params, 'to');
    }

    public function findDataByKey($array, $key)
    {
        $data = [];

        foreach ($array as $value ) {
            if (is_string($value[$key])) {
                $data[] = $value[$key];
            } else {
                foreach ($value[$key] as $value) {
                    $data[] = $value;
                }
            }
        }

        return $data;
    }

    private function asociarParametrosConRespuesta($params, $response)
    {
        $response['token_push_notification'] = $params['to'];
        return $response;
    }

    protected function getResponse()
    {
        $this->response_data= json_decode($this->response->getBody()->getContents(), true)['data'];
    }

    function is_assoc( $array ) {
        return array_keys( $array ) !== range( 0, count($array) - 1 );
    }
}
