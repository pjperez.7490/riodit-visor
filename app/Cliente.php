<?php

namespace App;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Cliente extends Model
{
    protected $guarded = ['id'];
    protected $with = ['creadoPor'];
    protected $appends = ['logo'];

    private function guardarLogo($fileFoto){
        $extension = $fileFoto->getClientOriginalExtension();
        $filename = uniqid();
        Storage::disk('clientes')->put($filename.'.'.$extension,  File::get($fileFoto));
        $urlfinal = $filename . '.' . $extension;

        return $urlfinal;
    }

    private function eliminarLogoUrl(){
        $logoUrl = $this->getOriginal('logo_url');
        return Storage::disk('clientes')->delete($logoUrl);
    }

    public function actualizar(array $options = []){
        $data = collect($options);

        if($this->exists){
            if( array_key_exists('logo_url', $data->toArray()) && array_key_exists('cambiar_imagen', $data->toArray())  ){
                $this->eliminarLogoUrl();
                $logoUrl = $this->guardarLogo($data['logo_url']) ;
                $data['logo_url'] = $logoUrl;
            }

            $data = $data->except(['_token', '_method', 'cambiar_imagen']);
        } 

        $this->update( $data->toArray() );
    }

    public function tieneImagen()
    {
        if ($this->logo_url == null | $this->logo_url == '') { return false ; }
        return true;
    }

    // mutators
    public function getLogoAttribute()
    {
        $value = $this->logo_url;
        if ($value == null | $value == "") {
            return url("/img/riodit.png");
        }

        return url("/storage/clientes").'/'.$value;
    }

    // relationships
    public function usuarios()
    {
        return $this->hasMany(User::class);
    }

    public function creadoPor()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    public function carpetas()
    {
        return $this->hasMany(Carpeta::class);
    }

    // scope
    public function scopeOrderByNombre($query)
    {
        return $query->orderBy('nombre');
    }

    // events
    public static function boot()
    {
        parent::boot();

        self::creating(function($cliente){
            if ($cliente->logo_url) {
                $logoUrl = $cliente->guardarLogo($cliente->logo_url);
                $cliente->logo_url = $logoUrl;
            }
        });

        self::deleted(function($cliente){
            $cliente->eliminarLogoUrl();
        });
    }
}
