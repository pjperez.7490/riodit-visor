<?php

namespace App\Console\Commands;

use Exception;
use App\EstadoCarpeta;
use App\Services\Expo;
use Illuminate\Console\Command;

class NotificacionesPush extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notificacionesPush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $expo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Expo $expo)
    {
        parent::__construct();
        $this->expo = $expo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $estadoSinNotificar = EstadoCarpeta::where('notificado', false)->get();

        $notifications = [];
        foreach ($estadoSinNotificar as $estadoSN) {
            try {
                // tengo a todos los usuarios que tengo q notificar.
                // codigo necesario para envia notificacion push a cada usuario.
                $notifications[] = $this->prepareNotification($estadoSN);
                $estadoSN->notificado = true;
                $estadoSN->save();
            } catch (Exception $e) {
                // si hubo error marco como que no envie notificacion.
                $estadoSN->notificado = false;
                $estadoSN->save();
            }
        }

        $response = $this->expo->sendNotification($notifications); 
    }

    private function prepareNotification($estadoCarpeta)
    {
        $tokensPushNotification = $estadoCarpeta
                                    ->carpeta
                                    ->cliente
                                    ->usuarios
                                    ->filter(function($user){return $user->pushNotifications->count(); })
                                    ->map(function($user){return $user->getTokens(); });

        $tokensPushNotification = $tokensPushNotification->toArray();

        $tokens = [];
        foreach ($tokensPushNotification as $key => $value) {
            $tokens = array_merge($tokens,$value );
        }

        return [
            'title' => 'Carpeta - '. $estadoCarpeta->carpeta->numero_carpeta,
            'sound' => 'default',
            'body'  => 'Estado: '. $estadoCarpeta->nombre_mostrar,
            'to'    => $tokens,
            'data'  => $estadoCarpeta->toArray(),
            '_displayInForeground' => true,
        ];
    }
}
