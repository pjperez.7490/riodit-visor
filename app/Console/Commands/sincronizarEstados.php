<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Webklex\IMAP\Client;
use Carbon\Carbon;
use App\EstadoCarpetaPreset;
use App\Carpeta;
use App\EstadoCarpeta;

class sincronizarEstados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sincronizarEstados';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {

        $aMessage = $this->getMensajes();
        //  var_dump( $aMessage);
        foreach ($aMessage as $oMessage) {
            $datos = $this->getDatos($oMessage);
            if ($datos != null) {
                $dato_duaCarpeta =  $this->getValor($datos, 'DUACarpeta');
                $dato_fechaIngreso = $this->getValor($datos, 'ObservacionFechaIngreso');
                $dato_errorCodigo = $this->getValor($datos, 'ErrorCodigo');
          //      var_dump($datos);
                if ($dato_duaCarpeta != null && $dato_fechaIngreso != null && $dato_errorCodigo != null) {
                    $numeroCarpeta =  $dato_duaCarpeta;
                    $codigo = $dato_errorCodigo;
    
                    $fecha = Carbon::createFromFormat('d/m/Y H:i', $dato_fechaIngreso);
                    $estadoPreset = EstadoCarpetaPreset::where('codigo_sistema_aduana', $codigo)->first();

                    if ($estadoPreset != null) {
                        $carpeta = Carpeta::where('numero_carpeta', $numeroCarpeta)->first();
                        if ($carpeta != null) {

                            $estadoCarpeta = EstadoCarpeta::where('carpeta_id', $carpeta->id)->where('estado_carpeta_preset_id', $estadoPreset->id)->first();
                            if ($estadoCarpeta == null) {

                                $estado = new EstadoCarpeta();
                                $estado->nombre_clave = $estadoPreset->nombre_clave;
                                $estado->nombre_mostrar = $estadoPreset->nombre_mostrar;
                                $estado->descripcion = $estadoPreset->descripcion;
                                $estado->fecha_evento = $fecha;
                                $estado->notificado = false;
                                $estado->carpeta_id = $carpeta->id;
                                $estado->estado_carpeta_preset_id = $estadoPreset->id;
                                $estado->user_id = null;
                                $estado->save();
                            }
                        }
                    }
                }
            }
        }
    }


    private function getMensajes()
    {
        $oClient = new Client();
        $oClient->connect();

        $oFolder = $oClient->getFolder(env('IMAP_NOTIFICACIONES_CARPETA'));
        $aMessage = $oFolder->query()->from(env('IMAP_NOTIFICACIONES_REMITENTE'))->UnSeen()->limit(50)->get();

        return $aMessage;
    }

    private function getDatos($oMessage)
    {
        $mensaje =  $oMessage->getHtmlBody();
        //  var_dump($mensaje);
        if (mb_strpos($mensaje, '<body') !== false) {
            $dentroBody = explode("</body>", explode("<body>", $mensaje)[1])[0];
            $lineas = explode("<br>", $dentroBody);
            // var_dump($lineas);
            $datos = array();
            foreach ($lineas as $linea) {
                $d = explode(":", $linea, 2);

                if (sizeof($d) >= 2) {
                    $d[0] = trim($d[0]);
                    $datos[] = $d;
                }
            }
            //    var_dump($datos);
            return $datos;
        }
        return null;
    }

    private function getValor($datos, $key)
    {
        foreach ($datos as $dato) {
            if ($dato[0] === $key) {
                return $dato[1];
            }
        }
        return null;
    }
}
